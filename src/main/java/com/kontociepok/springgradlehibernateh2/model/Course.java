package com.kontociepok.springgradlehibernateh2.model;

import java.util.*;

public class Course {

    private Long id;

    private String name;

    private String description;

    private Set<Long> studentsId = new TreeSet<>();

    public Course() {
    }

    public Course(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Course(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(id, course.id) && Objects.equals(name, course.name) && Objects.equals(description, course.description) && Objects.equals(studentsId, course.studentsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, studentsId);
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", students=" + studentsId +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Long> getStudentsId() {
        return studentsId;
    }

    public void addStudentId(Long studentId) {
        this.studentsId.add(studentId);
    }
}
